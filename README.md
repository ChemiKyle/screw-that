# Screw That
A python program to determine the radius and threading of a screw using opencv. 

## Imaging
User should place screw on a backlit surface.

## TODO:
[] Solve YUYV colorspace issue causing video to fail
[] Support multiple resolutions
[] Support alternate scale methods (single known screw, or other common object (e.g. a coin) to initialize instead of grid)
