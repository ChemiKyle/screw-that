import cv2
import numpy as np
from matplotlib import pyplot as plt

fil = '8-32_b'
# fil = 'my_photo-4'
img = cv2.imread('img/' + fil + '.jpg', 0)

def preproc(img):
    # img = cv2.cvtColor(img, cv2.COLOR_BAYER_BG2RGBA) causes issues if converted here
    blurred = cv2.GaussianBlur(img, (3, 3), 0)
    return blurred

# CCredit to pyimagesearch.com
def auto_canny(img, sigma=0.33):
    v = np.median(img)
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 - sigma) * v))

    edges = cv2.Canny(img, lower, upper)
    return edges

def try_edge(img, sigma = 0.33):
    grid = plt.GridSpec(2, 2)
    # plt.subplot(221)
    plt.subplot(grid[0,0])
    img = cv2.cvtColor(img, cv2.COLOR_BAYER_BG2RGBA)
    plt.imshow(img) # input image

    edges = auto_canny(preproc(img), sigma)
    # plt.subplot(222)
    plt.subplot(grid[0,1])
    plt.imshow(edges, cmap = 'gray') # edges only

    # plt.subplot(223)
    plt.subplot(grid[1,:])
    plt.imshow(img)
    edges = edges.astype(float)
    edges[edges == 0] = np.nan
    plt.imshow(edges, cmap='OrRd_r') # overlay edges on image

    plt.tight_layout()
    plt.show()


try_edge(img, 0.55)

def video():
    # cv2.cap.set(cv2.CAP_PROP_MODE, cv2.CAP_MODE_YUYV)
    cap = cv2.VideoCapture()
    while(True):
        ret, frame = cap.read()
        print(ret)

        img = preproc(frame)
        # img = frame
        cv2.imshow('frame', img)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    cap.release()
    cv2.destroyAllWindows()

# video()
